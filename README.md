PinYouShopping
==

## An online shopping website

* Homepage
![](screenshots/1.png)

* Goods List
![](screenshots/3.png)

* Product information
![](screenshots/2.png)

* Register
![](screenshots/4.png)